
const axios = require('axios');

const getClima = async(lat, lng) => {

    // let encodedUrl = encodeURI()
    
    // axios
    let resp = await axios.get(`http://api.openweathermap.org/data/2.5/weather?units=metric&lat=${ lat }&lon=${ lng }&appid=d7a85320ba8257b96265eabc8bae0951`);

    if( resp.data.cod != 200) {
        throw new Error(`ERROR!`);
    }
    // return resp ... main.temp
    
    let temp = resp.data.main.temp;

    return temp;
}


module.exports = {
    getClima
}